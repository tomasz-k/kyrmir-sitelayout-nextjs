/** @type {import('next').NextConfig} */

const nextConfig = {
  async rewrites() {
    return {
      beforeFiles: [
        {
          source: '/http_api/:path*',
          has: [
            {
              type: 'header',
              key: 'gothic_token',
              value: process.env.G2O_TOKEN,
            },
          ],
          destination: `http://${process.env.G2O_SERVER_ADDRESS}/:path*`,
        },
      ],
    };
  },
  async headers() {
    return [
      {
        source: '/api/:path*',
        headers: [
          { key: 'Access-Control-Allow-Credentials', value: 'true' },
          { key: 'Access-Control-Allow-Origin', value: '*' },
          {
            key: 'Access-Control-Allow-Methods',
            value: 'GET,DELETE,PATCH,POST,PUT',
          },
          {
            key: 'Access-Control-Allow-Headers',
            value:
              'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version',
          },
        ],
      },
    ];
  },
  webpack: (config) => {
    config.externals = [...config.externals, 'canvas'];
    return config;
  },
  images: {
    domains: ['cdn.discordapp.com', 'kyrmir.eu'],
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'api.kyrmir.eu',
        port: '',
        pathname: '/portal/uploads/**',
      },
    ],
  },
  output: 'standalone',
  reactStrictMode: false,
};

module.exports = nextConfig;
