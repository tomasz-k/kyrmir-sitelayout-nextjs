'use client';

import Script from 'next/script';

const GoogleAnalytics = () => (
  <>
    <Script src="https://www.googletagmanager.com/gtag/js?id=G-6MMWXLM3NB" />
    <Script id="google-analytics">
      {`
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-6MMWXLM3NB');
    `}
    </Script>
  </>
);

export default GoogleAnalytics;
