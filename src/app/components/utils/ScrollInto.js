'use client';

import Link from 'next/link';

export const scrollFunc = (nodeId, e = null) => {
  const element = document.getElementById(nodeId);
  if (element) {
    element.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }
  if (e) e.preventDefault();
};

export const ScrollInto = ({ children, to, prev = false, ...props }) => (
  <Link {...props} onClick={(e) => scrollFunc(to, prev && e)}>
    {children}
  </Link>
);
