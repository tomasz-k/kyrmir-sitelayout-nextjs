import Image from 'next/image';
import styles from '@/app/styles/discord.module.css';

const getWidget = async (widgetId) => {
  const res = await fetch(
    `https://discord.com/api/guilds/${widgetId}/widget.json`,
    { next: { revalidate: 360 } }
  );
  if (!res.ok) {
    throw new Error('Failed to fetch widget');
  }
  return res.json();
};

const Discord = async ({ widgetId }) => {
  const widget = await getWidget(widgetId);
  return (
    widget && (
      <div className={styles.discord}>
        <header className={styles.header}>
          <h2 className={styles.title}>
            Discord -&nbsp;
            <span className={styles.title__count}>
              {widget.presence_count} Members online
            </span>
          </h2>
        </header>
        <div className={styles.list}>
          <span className={styles.list__title}>Members online</span>
          <ul className={styles.list__ul}>
            {widget.members?.map((member) => (
              <li key={member.id} className={styles.list__item}>
                <Image
                  alt="avatar"
                  width={0}
                  height={0}
                  className={styles.list__img}
                  src={member.avatar_url}
                />
                <span className={styles.list__username}>{member.username}</span>
              </li>
            ))}
          </ul>
        </div>
        <div className={styles.joinus}>
          <p className={styles.joinus__text}>Hangout with people who get it</p>
          <div className={styles.joinus__container}>
            <a
              className={styles.joinus__button}
              rel="noreferrer"
              target="_blank"
              href={widget.instant_invite}
            >
              Join Discord
            </a>
          </div>
        </div>
      </div>
    )
  );
};

export default Discord;
