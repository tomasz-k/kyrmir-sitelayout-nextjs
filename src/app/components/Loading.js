'use client';

import styles from '@/app/styles/section.module.css';

const Loading = () => (
  <section className={styles.section}>
    <header className={styles.header}>
      <h2 className={styles.title}>Loading feed...</h2>
    </header>
  </section>
);

export default Loading;
