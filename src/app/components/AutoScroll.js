'use client';

import { useEffect } from 'react';
import { usePathname } from 'next/navigation';

import { scrollFunc } from '@/app/components/utils/ScrollInto';

const AutoScroll = () => {
  const pathname = usePathname();
  useEffect(() => {
    setTimeout(() => {
      if (pathname !== '/') {
        scrollFunc('main');
      }
    }, 300);
  }, []);
};

export default AutoScroll;
