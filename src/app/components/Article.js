import Image from 'next/image';

import parse from 'html-react-parser';
import { ScrollInto } from '@/app/components/utils/ScrollInto';

import styles from '@/app/styles/article.module.css';

const Article = ({ id, content }) => {
  const replaceImage = {
    replace: ({ name, attribs }) => {
      if (name === 'img') {
        return (
          <Image
            className="ckeditor"
            src={attribs.src}
            alt={attribs.alt}
            width={400}
            height={300}
          />
        );
      }
    },
  };
  return (
    content && (
      <article key={id} className={styles.article}>
        <header className={styles.header}>
          <h3 className={styles.title}>
            <ScrollInto
              className={styles.link}
              to={'main'}
              href={`/archive/${content.id}`}
            >
              {content.id}. {content.attributes.title}
            </ScrollInto>
          </h3>
        </header>
        <div className={'ckeditor'}>
          {parse(content.attributes.text, replaceImage)}
        </div>
        <footer className={styles.footer}>
          <p className={styles.author}>
            Authored by&nbsp;
            <span rel="noreferrer" target="_blank" className={styles.mark}>
              {content.attributes.author}
            </span>&nbsp;
            at {content.attributes.date}
          </p>
        </footer>
      </article>
    )
  );
};

export default Article;
