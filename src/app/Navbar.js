'use client';

import Link from 'next/link';
import Image from 'next/image';

import logo from '@/assets/kyrmir_logo.webp';
import styles from '@/app/styles/navbar.module.css';

import { useState } from 'react';
import { ScrollInto } from '@/app/components/utils/ScrollInto';

const Nav = () => {
  const [toggle, setToggle] = useState(false);
  return (
    <nav className={styles.navbar}>
      <div className={styles.container}>
        <div className={styles.topbox}>
          <Link
            onClick={() => scrollFunc('main')}
            aria-label="scroll up"
            className={[styles.link, styles.link__logo].join(' ')}
            href="/"
          >
            <Image
              width={110}
              height={40}
              alt="home"
              src={logo}
              className={styles.logo}
            />
          </Link>
          <button
            aria-label="toggle menu mobile"
            onClick={() => setToggle(!toggle)}
            className={
              !toggle
                ? styles.toggle
                : [styles['toggle'], styles['toggle--active']].join(' ')
            }
          >
            <span className={styles.toggle__line}></span>
            <span className={styles.toggle__line}></span>
            <span className={styles.toggle__line}></span>
          </button>
        </div>
        <div
          className={
            !toggle
              ? styles.listbox
              : [styles['listbox'], styles['listbox--active']].join(' ')
          }
        >
          <ol className={styles.list}>
            <li className={styles.item}>
              <ScrollInto to={'main'} className={styles.link} href="/">
                News
              </ScrollInto>
            </li>
            <li className={styles.item}>
              <ScrollInto to={'main'} className={styles.link} href="/about">
                About us
              </ScrollInto>
            </li>
            <li className={styles.item}>
              <ScrollInto
                to={'social'}
                className={styles.link}
                href="#social"
                prev={true}
              >
                Links
              </ScrollInto>
            </li>
            <li className={styles.item}>
              <ScrollInto to={'main'} className={styles.link} href="/teamdm">
                TDM
              </ScrollInto>
            </li>
            <li className={styles.item}>
              <ScrollInto
                to={'main'}
                className={styles.link}
                href="/leaderboard"
              >
                Leaderboard
              </ScrollInto>
            </li>
            <li className={[styles.item, styles.item__play].join(' ')}>
              <ScrollInto
                to={'play'}
                className={styles.play}
                href="#play"
                prev={true}
              >
                How to play?
              </ScrollInto>
            </li>
          </ol>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
