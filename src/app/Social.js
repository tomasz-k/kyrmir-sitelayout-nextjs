import styles from '@/app/styles/social.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  faDiscord,
  faFacebook,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons';

import { faCircleDollarToSlot } from '@fortawesome/free-solid-svg-icons';

const Social = () => (
  <section className={styles.section} id="social">
    <div className={styles.container}>
      <header className={styles.header}>
        <h2 className={styles.header__title}>Useful links</h2>
      </header>
      <div className={styles.media}>
        <div className={styles.media__row}>
          <span className={styles.media__stack}>
            <a
              name="discord"
              className={styles.media__link}
              aria-label="discord invite"
              href="https://discord.gg/aSnJWHgndf"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faDiscord} />
            </a>
          </span>
          <span className={styles.media__stack}>
            <a
              name="facebook"
              className={styles.media__link}
              aria-label="facebook link"
              href="https://www.facebook.com/people/Kyrmir-MMO-RPG/100056911635081/"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faFacebook} />
            </a>
          </span>
        </div>
        <div className={styles.media__row}>
          <span className={styles.media__stack}>
            <a
              name="youtube"
              className={styles.media__link}
              aria-label="official youtube"
              href="https://www.youtube.com/@kyrmirmmorpg2404"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faYoutube} />
            </a>
          </span>
          <span className={styles.media__stack}>
            <a
              name="patronite"
              className={styles.media__link}
              aria-label="official patronite"
              href="https://patronite.pl/Kyrmir"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faCircleDollarToSlot} />
            </a>
          </span>
        </div>
      </div>
    </div>
  </section>
);

export default Social;
