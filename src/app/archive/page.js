import { ScrollInto } from '@/app/components/utils/ScrollInto';

import styles from '@/app/styles/section.module.css';

const getArchive = async () => {
  const res = await fetch(
    'https://api.kyrmir.eu/portal/api/articles?sort[0]=id:desc&pagination&fields[0]=title&fields[1]=date',
    { next: { revalidate: 180 } }
  );
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const Archive = async () => {
  const archiveData = await getArchive();
  const archive = archiveData.data;
  return (
    <section className={styles.section}>
      <header className={styles.header}>
        <h2 className={styles.title}>ARCHIVE - ARTICLE LIST</h2>
      </header>
      <div className={styles.container}>
        <ul className={styles.list} style={{ textAlign: 'center' }}>
          {archive?.map((article, index) => (
            <li
              key={index}
              className={styles.list__item}
              style={{ fontSize: '1.15rem' }}
            >
              <ScrollInto
                className={styles.link}
                to={'main'}
                href={`/archive/${article.id}`}
              >
                {article.id}. {article.attributes.title} from&nbsp;
                {article.attributes.date}
              </ScrollInto>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default Archive;
