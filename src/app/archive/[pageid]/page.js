import { notFound } from 'next/navigation';

import Article from '@/app/components/Article';

import styles from '@/app/styles/section.module.css';

const getPage = async (pageid) => {
  const res = await fetch(
    `https://api.kyrmir.eu/portal/api/articles?filters[id][$eq]=${pageid}`,
    { next: { revalidate: 180 } }
  );
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const Page = async ({ params }) => {
  const pageData = await getPage(params.pageid);
  if (!pageData || pageData.data.length === 0) {
    notFound();
  }
  const article = pageData.data[0];
  return (
    <section className={styles.section}>
      <header className={styles.header}>
        <h2 className={styles.title}>ARCHIVE - ARTICLE</h2>
      </header>
      <div className={styles.container}>
        <Article content={article} />
      </div>
    </section>
  );
};

export default Page;
