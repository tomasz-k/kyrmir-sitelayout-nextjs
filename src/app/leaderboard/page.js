import styles from '@/app/styles/leaderboard.module.css';

import TableLeaderboard from '@/app/leaderboard/TableLeaderboard';

const Leaderboard = () => {
  return (
    <section className={styles.section}>
      <header className={styles.header}>
        <h2 className={styles.title}>Leaderboard</h2>
      </header>
      <div className={styles.container}>
        <TableLeaderboard />
      </div>
    </section>
  );
};

export default Leaderboard;
