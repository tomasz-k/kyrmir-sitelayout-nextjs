'use client';

import styles from '@/app/styles/leaderboard_table.module.css';

import {
  useInfiniteQuery,
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query';
import { useEffect, useRef } from 'react';

const queryClient = new QueryClient();

const fetchData = async (page) => {
  const res = await fetch(`https://api.kyrmir.eu/player/page/${page}/20`);
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const TableLocal = () => {
  const tableRef = useRef(null);
  const { data, fetchNextPage, isFetchingNextPage, isLoading } =
    useInfiniteQuery(
      ['query'],
      async ({ pageParam = 1 }) => await fetchData(pageParam),
      {
        getNextPageParam: (_, pages) => pages.length + 1,
      }
    );
  useEffect(() => {
    const observer = new IntersectionObserver(() => fetchNextPage());
    if (tableRef.current) {
      observer.observe(tableRef.current);
    }
  }, [tableRef]);
  return (
    <table className={styles.table}>
      <tbody className={styles.body}>
        <tr
          className={[
            styles['row'],
            styles['row--none'],
            styles['row--head'],
          ].join(' ')}
        >
          <th className={styles.cell}>No.</th>
          <th className={styles.cell}>Name</th>
          <th className={styles.cell}>Level</th>
          <th className={styles.cell}>Exp</th>
          <th className={styles.cell}>Guild</th>
          <th className={styles.cell}>Online time</th>
          <th className={styles.cell}>Status</th>
        </tr>
        {data &&
          data.pages?.map((page, i) =>
            page?.map((player, j) => {
              const index = i * 20 + (j + 1);
              return (
                <tr key={index} className={styles.row}>
                  <td className={styles.cell}>{index}</td>
                  <td className={styles.cell}>{player.username}</td>
                  <td className={styles.cell}>{player.level}</td>
                  <td className={styles.cell}>{player.experience}</td>
                  <td className={styles.cell}>
                    {player.guild ? player.guild : 'No guild'}
                  </td>
                  <td className={styles.cell}>{player.online_time}</td>
                  <td className={styles.cell}>
                    {player.online ? 'Online' : 'Offline'}
                  </td>
                </tr>
              );
            })
          )}
      </tbody>
      <tfoot className={styles.foot} ref={tableRef}>
        {(isFetchingNextPage || isLoading) && (
          <tr className={[styles['row'], styles['row--none']].join(' ')}>
            <td colSpan={7} className={styles.cell}>
              Trying to fetch data...
            </td>
          </tr>
        )}
      </tfoot>
    </table>
  );
};

const TableLeaderboard = () => (
  <QueryClientProvider client={queryClient}>
    <TableLocal />
  </QueryClientProvider>
);

export default TableLeaderboard;
