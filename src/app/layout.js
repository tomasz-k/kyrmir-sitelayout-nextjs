import '@/app/styles/globals.css';
import '@/app/styles/ckeditor.css';

import { Coda } from 'next/font/google';

import '@fortawesome/fontawesome-svg-core/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core';

config.autoAddCss = false;

const coda = Coda({
  subsets: ['latin'],
  weight: ['400', '800'],
});

import { PageWrapper } from '@/app/components/PageTransition';
import AutoScroll from '@/app/components/AutoScroll';
import GoogleAnalytics from '@/app/components/GoogleAnalytics';

import Hero from '@/app/Hero';
import Navbar from '@/app/Navbar';
import Tutorial from '@/app/Tutorial';
import Footer from '@/app/Footer';
import Social from '@/app/Social';

export const viewport = {
  width: 'device-width',
  initialScale: 1,
  maximumScale: 2,
  themeColor: [
    { media: '(prefers-color-scheme: light)', color: '#131313' },
    { media: '(prefers-color-scheme: dark)', color: '#131313' },
  ],
};

export const metadata = {
  icons: {
    icon: '/favicon.ico',
  },
  title: 'Kyrmir MMORPG - Gothic Online Server',
  description:
    'Welcome to the Kyrmir MMORPG and TDM servers! We are the oldest network of Gothic Online servers that has been running continuously since 2013. Join our community and create your own history with us!',
  keywords:
    'gothic-online, gothic-multiplayer, gothic 2, gothic, mmo, mmorpg, kyrmir 2, kyrmir serwer, kyrmir, gothic kyrmir, kyrmir forum, kyrmir wiki',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={coda.className}>
        <Hero />
        <main>
          <Navbar />
          <div id="main">
            <PageWrapper>{children}</PageWrapper>
          </div>
          <Social />
          <Tutorial />
        </main>
        <Footer />
      </body>
      <GoogleAnalytics />
      <AutoScroll />
    </html>
  );
}
