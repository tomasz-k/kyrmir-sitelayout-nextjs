import Image from 'next/image';

import styles from '@/app/styles/section.module.css';
import logo from '@/assets/tdm_logo.webp';

const TeamDM = () => (
  <section className={styles.section}>
    <header className={styles.header}>
      <h2 className={styles.title}>SERVER TDM</h2>
    </header>
    <div className={styles.container}>
      <div style={{ textAlign: 'center', width: '80%', margin: '0 auto' }}>
        <div className="section-universal__block">
          <Image
            width={600}
            height={350}
            className={styles.logo}
            src={logo}
            alt="project logo"
            priority={true}
            placeholder="blur"
          />
          <h3 className={styles.h3}>
            Warmly welcome to all fans and enthusiasts of the Gothic series!
          </h3>
        </div>
        <p className={styles.p}>
          Our team consists of two people. We have been playing Gothic
          Multiplayer fan projects for over a decade. After all this wonderful
          time, we have created our own vision of what the perfect server should
          look like.
        </p>
        <p className={styles.p}>
          Our server gives players the opportunity to participate in massive
          battles between factions, upgrade their stats as the game progresses,
          and acquire new equipment to become the strongest warrior in the
          kingdom!
        </p>
        <p className={styles.p}>
          Become a powerful warrior wielding a one-handed or two-handed sword,
          become an accurate archer or crossbowman, and if that doesn&apost suit
          you, there is always the path of the mage. Our server has it all!
        </p>
        <p className={styles.p}>
          We cannot forget about battling dangerous demons and legendary beasts,
          defeating which, the player would receive the most potent items. We
          have debated this for a long time and have come to the conclusion that
          we cannot let all these ideas go to waste.
        </p>
        <p className={styles.p}>
          After many months of tireless work, we are finally ready to announce
          our project: Kyrmir Team Deathmatch.
        </p>
        <div className={styles.block}>
          <iframe
            title="video_0"
            style={{ width: '80%', height: '40vh' }}
            frameBorder="0"
            data-cookieconsent="ignore"
            src="https://www.youtube.com/embed/d3rA5st-lRQ"
          />
          <iframe
            title="video_1"
            style={{ width: '80%', height: '40vh' }}
            frameBorder="0"
            data-cookieconsent="ignore"
            src="https://www.youtube.com/embed/tY_g-cmf2Ns"
          />
        </div>
        <p className={styles.p}>
          Brush off the dust from your swords. Draw the string of your bow.
          Prepare your magical runes. Return to the adventure once again, be the
          hero that your Camp needs!
        </p>
      </div>
    </div>
  </section>
);

export default TeamDM;
