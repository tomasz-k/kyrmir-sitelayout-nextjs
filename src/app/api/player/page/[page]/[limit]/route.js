import { service } from '@/app/api/player/service';

export async function GET(request, { params }) {
  const page = parseInt(params.page),
    limit = parseInt(params.limit);

  if (!page || !limit) {
    return new Response(null, {
      status: 400,
    });
  }

  const playerList = await service.findPlayerByPage(page, limit);

  if (!playerList) {
    return new Response(null, { status: 204 });
  }

  return Response.json(playerList.map(({ salt, password, ...rest }) => rest));
}
