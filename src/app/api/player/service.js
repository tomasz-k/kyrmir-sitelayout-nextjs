import { prisma } from '@/db/prisma';

BigInt.prototype['toJSON'] = function () {
  return parseInt(this.toString());
};

class ApiService {
  constructor() {}

  async getPlayerList() {
    return await prisma.player.findMany();
  }

  async getSizeByLimit(limit) {
    const count = await prisma.player.count();
    if (count == 0) return 0;

    return Math.ceil(count / limit);
  }

  async findPlayerByLimit(limit) {
    const playerList = await prisma.player.findMany({
      take: limit,
      orderBy: [
        {
          level: 'desc',
        },
        {
          experience: 'desc',
        },
      ],
    });

    return playerList;
  }

  async findPlayerByPage(page, limit) {
    if (page <= 0) return null;
    const calc = page * limit;

    const playerList = await prisma.player.findMany({
      take: limit,
      skip: calc - limit,
      orderBy: [
        {
          level: 'desc',
        },
        {
          experience: 'desc',
        },
      ],
    });

    return playerList;
  }

  async findPlayerByUsername(username) {
    const player = await prisma.player.findMany({
      where: {
        username: {
          contains: username,
        },
      },
      take: 5,
      include: {
        player_eq: {
          select: {
            instance: true,
            amount: true,
          },
        },
      },
    });

    return player;
  }
}

export const service = new ApiService();
