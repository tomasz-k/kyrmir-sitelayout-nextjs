import { service } from '@/app/api/player/service';

export async function GET(request, { params }) {
  const playerList = await service.findPlayerByUsername(params.username);

  if (!playerList) {
    return new Response(null, { status: 204 });
  }

  return Response.json(playerList.map(({ salt, password, ...rest }) => rest));
}
