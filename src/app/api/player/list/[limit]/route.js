import { service } from '@/app/api/player/service';

export async function GET(request, { params }) {
  const limit = parseInt(params.limit);

  if (!limit) {
    return new Response(null, { status: 400 });
  }

  const playerList = await service.findPlayerByLimit(limit);

  if (!playerList) {
    return new Response(null, { status: 204 });
  }

  return Response.json(playerList.map(({ salt, password, ...rest }) => rest));
}
