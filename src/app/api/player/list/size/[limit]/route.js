import { service } from '@/app/api/player/service';

export async function GET(request, { params }) {
  const limit = parseInt(params.limit);

  if (!limit) {
    return new Response(null, { status: 400 });
  }

  const listSize = await service.getSizeByLimit(limit);

  if (!listSize) {
    return new Response(null, { status: 204 });
  }

  return Response.json(listSize);
}
