import { service } from '@/app/api/player/service';

export async function GET() {
  const playerList = await service.getPlayerList();

  if (!playerList) {
    return new Response(null, { status: 204 });
  }

  return Response.json(playerList.map(({ salt, password, ...rest }) => rest));
}
