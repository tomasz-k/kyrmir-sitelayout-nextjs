import styles from '@/app/styles/section.module.css';

const Leaderboard = () => {
  return (
    <section className={styles.section}>
      <header className={styles.header}>
        <h2 className={styles.title}>API entry points</h2>
      </header>
      <div className={styles.container}>
        <div>
          <div className={styles.box}>
            <h2>{'https://api.kyrmir.eu/player/list/'}</h2>
            <p className={styles.box__p}>Returns all players data</p>
          </div>
          <div className={styles.box}>
            <h2>{'https://api.kyrmir.eu/player/list/{limit}'}</h2>
            <p className={styles.box__p}>
              Returns players data within given limit
            </p>
          </div>
          <div className={styles.box}>
            <h2>{'https://api.kyrmir.eu/player/page/{page}/{limit}'}</h2>
            <p className={styles.box__p}>
              Returns players data within given page and limit
            </p>
          </div>
          <div className={styles.box}>
            <h2>{'https://api.kyrmir.eu/player/name/{username}'}</h2>
            <p className={styles.box__p}>
              Returns players data by given username
            </p>
          </div>
          <div className={styles.box}>
            <h2>{'https://api.kyrmir.eu/http_api/{path}'}</h2>
            <p className={styles.box__p}>
              Proxy for&nbsp;
              <a
                className={styles.box__a}
                href="https://gitlab.com/tomasz-k/httpserver_g2o"
              >
                G2O http server
              </a>
              , currently provides access: status, players
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Leaderboard;
