import styles from '@/app/styles/home.module.css';

import { ScrollInto } from '@/app/components/utils/ScrollInto';
import Article from '@/app/components/Article';
import Discord from '@/app/components/Discord';

import ServerStatus from '@/app/ServerStatus';
import Top from '@/app/Top';

const getArticles = async () => {
  const res = await fetch(
    'https://api.kyrmir.eu/portal/api/articles?sort[0]=id:desc&pagination[limit]=5',
    { next: { revalidate: 180 } }
  );
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const Home = async () => {
  const articlesData = await getArticles();
  const articles = articlesData.data;
  return (
    <div className={styles.portal}>
      <section className={styles.section}>
        <header className={styles.section__header}>
          <h2 className={styles.section__title}>NEWS</h2>
        </header>
        {articles && (
          <div className={styles.container}>
            {articles.map((article, index) => (
              <>
                <Article id={index} content={article} />
                <hr className={styles.break} />
              </>
            ))}
            <div className={styles.section__footer}>
              {articles.length >= 5 ? (
                <ScrollInto
                  className={styles.button}
                  to={'main'}
                  href="/archive"
                >
                  VISIT OUR ARCHIVE
                </ScrollInto>
              ) : null}
            </div>
          </div>
        )}
      </section>
      <aside className={styles.side}>
        <div className={styles.container__side}>
          <div className={styles.side__group}>
            <ServerStatus />
          </div>
          <div className={styles.side__group}>
            <Top />
          </div>
          <div className={styles.side__group}>
            <Discord widgetId={'272849408859570176'} />
            {/*1037077923284389920 - TDM*/}
          </div>
        </div>
      </aside>
    </div>
  );
};

export default Home;
