import styles from '@/app/styles/top.module.css';

const getTop = async () => {
  const res = await fetch('https://api.kyrmir.eu/player/list/10', {
    next: { revalidate: 180 },
  });
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const Top = async () => {
  const top = await getTop();
  return (
    top && (
      <div className={styles.top}>
        <header className={styles.header}>
          <h2 className={styles.title}>Leaderboard</h2>
        </header>
        <table className={styles.table}>
          <tbody>
            <tr className={styles.table__row}>
              <th className={styles.table__cell}>No.</th>
              <th className={styles.table__cell}>Name</th>
              <th
                className={[
                  styles['table__cell'],
                  styles['table__cell--mobile'],
                ].join(' ')}
              >
                Level
              </th>
            </tr>
            {top.map((player, index) => (
              <tr key={index} className={styles.table__row}>
                <td className={styles.table__cell}>{index + 1}</td>
                <td className={styles.table__cell}>{player.username}</td>
                <td
                  className={[
                    styles['table__cell'],
                    styles['table__cell--mobile'],
                  ].join(' ')}
                >
                  {player.level}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  );
};

export default Top;
