import Image from 'next/image';
import { ScrollInto } from '@/app/components/utils/ScrollInto';

import logo from '@/assets/kyrmir_logo.webp';
import styles from '@/app/styles/hero.module.css';

const Hero = () => {
  return (
    <header className={styles.hero} id="headers">
      <div className={styles.container}>
        <Image
          width={700}
          height={200}
          className={styles.logo}
          src={logo}
          alt="site logo"
          priority={true}
          placeholder="blur"
        />
        <div className={styles.hello__message}>
          <h1 className={styles.title}>
            Welcome to the&nbsp;
            <span className={styles.title__mark}>Kyrmir MMORPG</span> and&nbsp;
            <span className={styles.title__mark}>TDM</span> servers!
          </h1>
          <h2 className={styles.text}>
            We are the oldest network of&nbsp;
            <span className={styles.title__mark}>Gothic Online</span> and&nbsp;
            <span className={styles.title__mark}>Gothic Multiplayer</span>&nbsp;
            servers that has been running continuously since 2013!
            <br />
            Join our community and create your own history with us!
          </h2>
        </div>
        <ScrollInto to={'main'} aria-label="scroll down" href="#" prev={true}>
          <span className={styles.jump}>
            <span className={styles.arrow}></span>
          </span>
        </ScrollInto>
      </div>
    </header>
  );
};

export default Hero;
