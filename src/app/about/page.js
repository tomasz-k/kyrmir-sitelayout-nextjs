import styles from '@/app/styles/section.module.css';

const About = () => (
  <section className={styles.section}>
    <header className={styles.header}>
      <h2 className={styles.title}>ABOUT US</h2>
    </header>
    <div className={styles.container}>
      <div className={styles.text}>
        <p className={styles.p}>
          The history of the Kyrmir MMORPG server dates back to 2014. In
          February it was formed on the initiative of a user nicknamed&nbsp;
          <span className={styles.mark}>KimiorV</span>, the project used the
          Mordan Engine, which was constantly developed and improved.
        </p>
        <p className={styles.p}>
          Kyrmir MMORPG is the only server that has stood the test of time on
          the Gothic Multiplayer platform and still exists today.
        </p>
        <p className={styles.p}>
          Back in 2015, it was the longest running institution on the GMP
          platform.The idea of the server is to play together with friends in an
          mmorpg edition of Gothic.
        </p>
        <h3 className={styles.h3}>Server timeline</h3>
        <ul className={[styles.list, styles['list--gill']].join(' ')}>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>02.2014</span> - First
            Edition of the Kyrmir MMORPG server managed by user&nbsp;
            <span className={styles.mark}>KimiorV</span>.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>06.2014</span> - Second
            Edition of the server, managed by users&nbsp;
            <span className={styles.mark}>Profesores</span> and&nbsp;
            <span className={styles.mark}>ArathornII</span>.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>10.2014</span> -
            Takeover of the project by the existing Event Management of the
            server user <span className={styles.mark}>TheDual</span>.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>23.11.2014</span>&nbsp;
            Creation of official fanpage on facebook&nbsp;
            <a
              href="https://www.facebook.com/profile.php?id=100056911635081"
              alt="facebook"
            >
              - link
            </a>
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>24.12.2014</span>&nbsp;
            Launching the server for the Christmas period with minor changes.
            Announcement of work on the actual launch under the care of the new
            team.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>08.03.2015</span> One
            of the most important dates in the history of Kyrmir. Start of the
            first official edition with the new team.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>01.11.2015</span>&nbsp;
            Preparations for the launch of the new edition of the server began.
            The launch took place on 15.01.2016.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>23.07.2016</span> A
            major update was released introducing a lot of content and
            activities for players.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>25.12.2016</span>&nbsp;
            Another major server update codenamed 2.1 was released, further
            expanding the portfolio offered on the server.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>(2017)</span> During
            2017, the Kyrmir server received a dedicated launcher, and an
            improved unofficial version of Gothic Multiplayer codenamed R-12 The
            official Discord of the server was also created. But that&apos:s not
            all, besides that, there is a new version of the website available
            at kyrmir.eu. Kyrmir has also changed its hosting provider, which
            has increased its stability and performance. Player activity is
            steadily increasing. Giveaways on Discord, and promotional codes are
            appearing. Most of the key systems are being built this year.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>18.05.2018</span> - A
            new edition begins on the server, and the old one, which lasted 664
            days, comes to an end.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>19.07.2019</span> -
            Season #1 - Hard - a new challenge for players - begins.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>01.01.2020</span> - End
            of the #1 season on the hard server.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>16.01.2021</span> -
            Season #2 kicked off.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>01.01.2023</span> - End
            of #3 server edition. Completion of activities on the Gothic
            Multiplayer platform. Start of work on transferring the server to
            the Gothic 2 Online platform.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>12.02.2023</span> -
            First round of Kyrmir TDM in Gothic Online.
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>28.05.2023</span> - The
            last round of the Kyrmir TDM server before the holiday break. 17
            rounds were held. (Period 12.02.2023 - 28.05.2023)
          </li>
          <li className={styles.list__item}>
            <span className={styles['list__item--bold']}>Q3 2023</span> - Start
            of MMORPG server testing on the Gothic 2 Online platform.
          </li>
        </ul>
        <h3 className={styles.h3}>
          People who have made significant contributions to the project
        </h3>
        <ul className={[styles.list, styles['list--gill']].join(' ')}>
          <li className={styles.list__item}>
            Profesores (main administrator until 2014; script improvements,
            project management)
          </li>
          <li className={styles.list__item}>
            ArathornII (community administration, support for server updates)
          </li>
          <li className={styles.list__item}>
            Jaskier (project owner until 2015)
          </li>
          <li className={styles.list__item}>
            Bimbol (developer of Mordan Engine, Mordan MMORPG server, Bimbol
            Engine framework)
          </li>
          <li className={styles.list__item}>
            gGorng (Administrator, server addon management)
          </li>
          <li className={styles.list__item}>Hades (Script support)</li>
          <li className={styles.list__item}>
            Patrix (Script support, web development support)
          </li>
          <li className={styles.list__item}>
            Risen (Support with Gothic Multiplayer platform)
          </li>
          <li className={styles.list__item}>
            TheShield (Server updates, community management, Event Management
            support)
          </li>
          <li className={styles.list__item}>
            Cycek, Hoder, Dermondo, Songogu, Torlof, Templar (Support with
            server updates, testing)
          </li>
          <li className={styles.list__item}>
            Dasko (2D, 3D graphic designer, level designer)
          </li>
          <li className={styles.list__item}>
            Zlobi (Gamemaster, community management)
          </li>
          <li className={styles.list__item}>
            Savo (Gamemaster, community management)
          </li>
          <li className={styles.list__item}>
            Sawares (Gamemaster, community management)
          </li>
          <li className={styles.list__item}>
            Dzunek (Gamemaster, community management)
          </li>
          <li className={styles.list__item}>
            WojtaS (Gamemaster, community management, Event Management)
          </li>
          <li className={styles.list__item}>SmoK (Event Management)</li>
          <li className={styles.list__item}>
            Forget (Server Moderator, Event Management)
          </li>
          <li className={styles.list__item}>Pandrodor (Server Moderator)</li>
          <li className={styles.list__item}>Akores (Server Moderator)</li>
          <li className={styles.list__item}>Msciwy (Server Moderator)</li>
          <li className={styles.list__item}>Imholl (Server Moderator)</li>
          <li className={styles.list__item}>Janek (Forum Moderator)</li>
          <li className={styles.list__item}>ktos (Support)</li>
          <li className={styles.list__item}>Parias (Support)</li>
          <li className={styles.list__item}>Hajting (Support)</li>
          <li className={styles.list__item}>Grunt (Support)</li>
          <li className={styles.list__item}>Gawwik (Youtuber)</li>
          <li className={styles.list__item}>Kasy (Youtuber)</li>
        </ul>
        <h3 className={styles.h3}>
          People currently active in administration:
        </h3>
        <ul className={[styles.list, styles['list--gill']].join(' ')}>
          <li className={styles.list__item}>
            <span className={styles.mark}>TheDual</span> (Owner, balance design,
            level design, project management, community management, event
            management)
          </li>
          <li className={styles.list__item}>
            <span className={styles.mark}>KimiorV</span> (Artist programmer,
            title sponsor)
          </li>
          <li className={styles.list__item}>
            <span className={styles.mark}>Dark</span> (Balance design, resource
            management)
          </li>
        </ul>
      </div>
    </div>
  </section>
);

export default About;
