import styles from '@/app/styles/status.module.css';

const getStatus = async () => {
  const res = await fetch('http://api.kyrmir.eu/http_api/status', {
    cache: 'no-store',
    headers: {
      gothic_token: process.env.G2O_TOKEN,
    },
  });

  if (!res.ok) {
    return null;
  }

  return res.json();
};

const ServerStatus = async () => {
  const status = await getStatus();
  return (
    <div className={styles.stats}>
      <header className={styles.header}>
        <h2 className={styles.title}>Status</h2>
      </header>
      {status !== null ? (
        <p className={styles.p}>
          Online players: {status.count} / {status.max_slots}
        </p>
      ) : (
        <div>
          <p className={styles.p}>Server is offline!</p>
        </div>
      )}
    </div>
  );
};

export default ServerStatus;
