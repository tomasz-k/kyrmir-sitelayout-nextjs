import styles from '@/app/styles/tutorial.module.css';

const Tutorial = () => (
  <section className={styles.section} id="play">
    <div className={styles.container}>
      <header className={styles.header}>
        <h2 className={styles.header__title}>HOW TO JOIN THE GAME?</h2>
      </header>
      <div className={styles.tutorial}>
        <div className={styles.row}>
          <h3 className={styles.row__title}>PREREQUIREMENTS</h3>
          <p className={styles.text}>
            To be able to join the game, you need a minimum operating system of
            Windows 7 64-bit and Installed Gothic II game with the Night of the
            Raven expansion,
          </p>
          <h4 className={styles.row__title}>
            which you can purchase on platforms such as:
          </h4>
          <div className={styles.row}>
            <p className={styles.p_link}>
              <a
                className={styles.link}
                target="_blank"
                rel="noreferrer"
                href="https://store.steampowered.com/app/39510/Gothic_II_Gold_Edition/"
              >
                Steam
              </a>
            </p>
            <p className={styles.p_link}>
              <a
                className={styles.link}
                target="_blank"
                rel="noreferrer"
                href="https://www.gog.com/pl/game/gothic_2_gold_edition"
              >
                GOG
              </a>
            </p>
            <p className={styles.p_link}>
              <a
                className={styles.link}
                target="_blank"
                rel="noreferrer"
                href="https://www.g2a.com/pl/gothic-2-gold-edition-steam-key-global-i10000045440002"
              >
                G2A
              </a>
            </p>
          </div>
        </div>
        <div className={styles.row}>
          <h4 className={styles.row__title}>
            If you are using the original disk version, you need to follow these
            steps (If you have the Steam/GOG or similar version, skip to step 3)
          </h4>
        </div>
        <div className={styles.row}>
          <span className={styles.step}>Step 1</span>
          <p className={styles.text}>
            Install&nbsp;
            <a
              className={styles.link}
              target="_blank"
              rel="noreferrer"
              href="https://www.dropbox.com/s/9pijja5jjemmu9t/gothic2_fix-2.6.0.0-rev2.exe.zip?dl=0"
            >
              Fix 2.6
            </a>
          </p>
        </div>
        <div className={styles.row}>
          <span className={styles.step}>Step 2</span>
          <p className={styles.text}>
            Install&nbsp;
            <a
              className={styles.link}
              target="_blank"
              rel="noreferrer"
              href="https://github.com/GothicFixTeam/GothicFix/releases/download/v1.8/Gothic2_PlayerKit-2.8.exe"
            >
              Gothic 2 PlayerKit
            </a>
          </p>
        </div>
        <div className={styles.row}>
          <span className={styles.step}>Step 3</span>
          <p className={styles.text}>
            Download the&nbsp;
            <a
              className={styles.link}
              target="_blank"
              rel="noreferrer"
              href="https://gothic-online.com.pl/download"
            >
              Gothic 2 Online
            </a>&nbsp;
            platform installer in the latest version (first column)
          </p>
        </div>
        <div className={styles.row}>
          <span className={styles.step}>Step 4</span>
          <p className={styles.text}>
            Install Gothic 2 Online files to the main game folder. Make sure the
            path is correct.
          </p>
        </div>
        <div className={styles.row}>
          <span className={styles.step}>Step 5</span>
          <p className={styles.text}>
            Run the G2O Launcher application and search for the Kyrmir MMORPG
            server on the list.
          </p>
        </div>
        <div className={styles.row}>
          <span className={styles.step}>Step 6</span>
          <p className={styles.text}>
            Now you can double click the left mouse button. Joining the server
            should start.
            <br />
            If something goes wrong, you can ask for help on the DISCORD server.
          </p>
        </div>
      </div>
    </div>
  </section>
);

export default Tutorial;
