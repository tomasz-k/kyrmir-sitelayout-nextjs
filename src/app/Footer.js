import Link from 'next/link';

import { ScrollInto } from '@/app/components/utils/ScrollInto';

import styles from '@/app/styles/footer.module.css';

const Footer = () => {
  return (
    <footer className={styles.footer} id="footer">
      <div className={styles.authorbox}>
        <p className={styles.authorbox__text}>
          {new Date().getFullYear()} &copy;&nbsp;
          <a
            href="https://gitlab.com/tomasz-k"
            className={styles.link}
          >
            Kyrmir.eu
          </a>&nbsp;
          All rights reserved
        </p>
      </div>
      <nav className={styles.navbar}>
        <ol className={styles.list}>
          <li className={styles.item}>
            <ScrollInto to={'main'} className={styles.link} href="/">
              News
            </ScrollInto>
          </li>
          <li className={styles.item}>
            <ScrollInto to={'main'} className={styles.link} href="/about">
              About us
            </ScrollInto>
          </li>
          <li className={styles.item}>
            <ScrollInto
              to={'social'}
              className={styles.link}
              href="#social"
              prev={true}
            >
              Links
            </ScrollInto>
          </li>
          <li className={styles.item}>
            <ScrollInto to={'main'} className={styles.link} href="/teamdm">
              TDM
            </ScrollInto>
          </li>
          <li className={styles.item}>
            <ScrollInto to={'main'} className={styles.link} href="/leaderboard">
              Leaderboard
            </ScrollInto>
          </li>
        </ol>
      </nav>
    </footer>
  );
};

export default Footer;
