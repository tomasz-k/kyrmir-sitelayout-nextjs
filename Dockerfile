# BUILD
FROM node:18-alpine AS deps
RUN apk add --no-cache libc6-compat
WORKDIR /app

COPY package.json package-lock.json* ./

RUN npm ci

FROM node:18-alpine AS builder
WORKDIR /app

COPY . ./
COPY --from=deps /app/node_modules ./node_modules

RUN npx prisma db pull
RUN npx prisma generate

RUN npm run build

FROM node:18-alpine AS runner
WORKDIR /app

ENV NODE_ENV production

# Fix for sharp...
RUN npm i sharp

COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public

# Ensure www-data exists
RUN set -x ; \
  addgroup -g 82 -S www-data ; \
  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

COPY --from=builder --chown=www-data:www-data /app/.next/standalone ./
COPY --from=builder --chown=www-data:www-data /app/.next/static ./.next/static

USER www-data

CMD ["node", "server.js"]